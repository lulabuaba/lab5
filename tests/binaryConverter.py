# This is a Binary <--> Decimal converter
# The Binary will be a list of integers representing the number

# Binary to Decimal

import math

def binToDecList(bin):
    size = len(bin)
    dec = 0
    for pos in range(size):
        curDigit = bin[size-pos-1]
        curPow = 2**pos
        dec += (curDigit * curPow)
    return dec





# Decimal to Binary
def decToBinList(d):
    if d == 0:
        return [0]  # Special case for decimal 0

    bin = []
    numDigits = math.floor(math.log2(d)) + 1  # Calculate the number of digits using logarithm
    for pos in range(numDigits-1, -1, -1):
        curPow = 2**pos
  
        if d >= curPow:
            bin.append(1)
            d -= curPow
        else:
            bin.append(0)
    return bin

# Testing
print(binToDecList([1, 1, 0, 0, 1]))
print(decToBinList(25))

