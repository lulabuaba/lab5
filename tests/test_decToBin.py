from binaryConverter import decToBinList

def test_decToBinList_normal_case():
    decimal = 25
    expected_binary = [1, 1, 0, 0, 1]
    actual_binary = decToBinList(decimal)
    assert actual_binary == expected_binary

def test_decToBinList_powers_of_two():
    decimal = 8
    expected_binary = [1, 0, 0, 0]
    actual_binary = decToBinList(decimal)
    assert actual_binary == expected_binary

def test_decToBinList_zero_case():
    decimal = 0
    expected_binary = [0]
    actual_binary = decToBinList(decimal)
    assert actual_binary == expected_binary
