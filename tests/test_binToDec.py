from binaryConverter import binToDecList

def test_binToDecList():
    binary = [1, 1, 0, 0, 1]
    expected_decimal = 25
    actual_decimal = binToDecList(binary)
    assert actual_decimal == expected_decimal

test_binToDecList()